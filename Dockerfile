FROM golang:1.20-alpine as goimports

RUN go install golang.org/x/tools/cmd/goimports@latest
RUN which goimports

FROM ubuntu:20.04

COPY --from=goimports /go/bin/goimports /usr/local/bin/goimports
RUN which goimports

ARG GEN_DIR=/binary

RUN apt-get update && \
    apt-get install -y openjdk-11-jdk && \
    apt -y install maven && \
    apt -y install software-properties-common && \
    add-apt-repository ppa:deadsnakes/ppa && \
    apt -y install python3 && \
    apt-get install -y python3-pip

ADD intermediate.requirements /intermediate/
ADD intermediate.py /intermediate/
RUN pip3 install -r /intermediate/intermediate.requirements

# needs mounted dir with openapi.yaml as /openapi
RUN mkdir ${GEN_DIR}
ADD ./modules/openapi-generator-cli ${GEN_DIR}/modules/openapi-generator-cli
ADD ./modules/openapi-generator ${GEN_DIR}/modules/openapi-generator
ADD ./pom.xml ${GEN_DIR}
ADD ./config.json ${GEN_DIR}

RUN cd ${GEN_DIR} && \
    mvn -Dmaven.javadoc.skip=true -Dmaven.test.skip=true clean install && \
    cp ${GEN_DIR}/modules/openapi-generator-cli/target/openapi-generator-cli.jar ./generator.jar


COPY ./entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

# needs dir mounted as /generated to write output
RUN mkdir /generated

ENTRYPOINT ["entrypoint.sh"]

# docker build -t generator .
# docker run -v /path/to/intermediate.py/dir:/intermediate -v /path/to/openapi/dir:/openapi -v /path/where/to/store/generated/output:/generated generator generate
