#!/usr/bin/env bash

function renderHelpMessage() {
    echo "
Flags:
        --meta-for-lists             generate additional json meta for list responses
"
}

OPTIONS=""
PACKAGE_NAME=regources

function parseArgs {
   while [[ -n "$1" ]]
   do
       case "$1" in
           --generate-horizon-stuff | --meta-for-lists | --meta | --raw-formats-as-types)
               OPTIONS="$OPTIONS $1"
               ;;
           --package-name | -pkg) shift
               [[ -z "$1" ]] && echo "package name not specified" && exit 1
               PACKAGE_NAME=$1
               ;;
           *)
               echo "invalid option $1"
               renderHelpMessage
               exit 1
               ;;
       esac
       shift
   done
}

case "$1" in
    help)
        renderHelpMessage
        echo "and then goimports files in output dir"
        ;;
    generate) shift
        parseArgs $@
        python3 /intermediate/intermediate.py --path /openapi/openapi.yaml --out /intermediate.yaml && \
        java -jar /binary/generator.jar generate -c /binary/config.json ${OPTIONS} --skip-validate-spec --additional-properties packageName=${PACKAGE_NAME} -i /intermediate.yaml -g go -o /generated && \
        chmod 666 /generated/*.go && \
        goimports -w /generated/*.go
        ;;
esac
