package org.openapitools.codegen.regources;

import io.swagger.v3.oas.models.media.StringSchema;

public class RegourcesStringSchema extends StringSchema {
    private Boolean needsResponses = null;
    private Boolean jsonable = null;
    private Boolean needsMustMethods = null;
    private Boolean needsKeyMethods = null;
    private Boolean canBeRelation = null;

    public void setNeedsResponses(Boolean regource) {
        needsResponses = regource;
    }

    public Boolean getNeedsResponses() {
        return needsResponses;
    }

    public Boolean getNeedsMustMethods() {
        return needsMustMethods;
    }

    public void setNeedsMustMethods(Boolean needsMustMethods) {
        this.needsMustMethods = needsMustMethods;
    }

    public Boolean getJsonable() {
        return jsonable;
    }

    public void setJsonable(Boolean jsonable) {
        this.jsonable = jsonable;
    }

    public Boolean getNeedsKeyMethods() {
        return needsKeyMethods;
    }

    public void setNeedsKeyMethods(Boolean needsKeyMethods) {
        this.needsKeyMethods = needsKeyMethods;
    }

    public Boolean getCanBeRelation() {
        return canBeRelation;
    }

    public void setCanBeRelation(Boolean canBeRelation) {
        this.canBeRelation = canBeRelation;
    }
}
