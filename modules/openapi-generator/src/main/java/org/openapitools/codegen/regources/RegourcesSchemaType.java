package org.openapitools.codegen.regources;

public enum RegourcesSchemaType {
    KEY(RegourcesObjectSchema.class),
    RELATION(RegourcesObjectSchema.class),
    RELATION_COLLECTION(RegourcesObjectSchema.class),
    DETAILS(RegourcesObjectSchema.class),
    OBJECT(RegourcesObjectSchema.class),
    STRING(RegourcesObjectSchema.class),
    DEFAULT(RegourcesObjectSchema.class);

    private final Class<?> defaultImplementation;

    private RegourcesSchemaType(Class<?> defaultImplementation) {
        this.defaultImplementation = defaultImplementation;
    }

    public Class<?> getDefaultImplementation() {
        return defaultImplementation;
    }
}
