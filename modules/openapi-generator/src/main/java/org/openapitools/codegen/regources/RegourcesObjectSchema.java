package org.openapitools.codegen.regources;

import io.swagger.v3.oas.models.media.ArraySchema;
import io.swagger.v3.oas.models.media.ObjectSchema;

import java.util.ArrayList;
import java.util.List;

public class RegourcesObjectSchema extends ObjectSchema {
    private Boolean needsResponses = null;
    private Boolean jsonable = null;
    private Boolean needsMustMethods = null;
    private Boolean needsKeyMethods = null;
    private Boolean canBeRelation = null;
    private Boolean isDetails = false;
    private Boolean needsDBMethods = null;
    private Boolean generate = null;
    private String responsePlaceholder = "Response";
    private Boolean useRawFormatsAsTypes = false;

    public Boolean getUseRawFormatsAsTypes() {
        return useRawFormatsAsTypes;
    }

    public void setUseRawFormatsAsTypes(Boolean useRawFormatsAsTypes) {
        this.useRawFormatsAsTypes = useRawFormatsAsTypes;
    }

    public RegourcesObjectSchema() { super(); }

    public RegourcesObjectSchema(RegourcesSchemaType type) {
        super();
        this.populate(type);
    }

    public String getResponsePlaceholder() {
        return responsePlaceholder;
    }

    public void setResponsePlaceholder(String responsePlaceholder) {
        this.responsePlaceholder = responsePlaceholder;
    }

    public Boolean getGenerate() {
        return generate;
    }

    public void setGenerate(Boolean generate) {
        this.generate = generate;
    }

    public Boolean getNeedsDBMethods() {
        return needsDBMethods;
    }

    public void setNeedsDBMethods(Boolean needsDBMethods) {
        this.needsDBMethods = needsDBMethods;
    }

    public void setNeedsResponses(Boolean needsResponses) {
        this.needsResponses = needsResponses;
    }

    public Boolean getNeedsResponses() {
        return needsResponses;
    }

    public Boolean getNeedsMustMethods() {
        return needsMustMethods;
    }

    public void setNeedsMustMethods(Boolean needsMustMethods) {
        this.needsMustMethods = needsMustMethods;
    }

    public Boolean getJsonable() {
        return jsonable;
    }

    public void setJsonable(Boolean jsonable) {
        this.jsonable = jsonable;
    }

    public Boolean getNeedsKeyMethods() {
        return needsKeyMethods;
    }

    public void setNeedsKeyMethods(Boolean needsKeyMethods) {
        this.needsKeyMethods = needsKeyMethods;
    }

    public Boolean getCanBeRelation() {
        return canBeRelation;
    }

    public void setCanBeRelation(Boolean canBeRelation) {
        this.canBeRelation = canBeRelation;
    }

    public Boolean isDetails() {
        return isDetails;
    }

    public void setIsDetails(Boolean details) {
        isDetails = details;
    }

    private void populate(RegourcesSchemaType schemaType) {
        if (schemaType == null) {
            return;
        }

        List<String> keyRequired = new ArrayList<>();
        keyRequired.add("id");
        keyRequired.add("type");

        RegourcesObjectSchema keySchema = new RegourcesObjectSchema();

        keySchema.addProperties("id", new RegourcesStringSchema())
                 .addProperties("type", new RegourcesStringSchema());

        keySchema.getProperties().get("id").setName("ID");
        keySchema.getProperties().get("type").set$ref("#/components/schemas/ResourceType");
        keySchema.setRequired(keyRequired);
        keySchema.set$ref("#/components/schemas/Key");
        keySchema.setType("object");
        keySchema.setNeedsKeyMethods(true);
        keySchema.setCanBeRelation(true);

        RegourcesObjectSchema linksSchema = new RegourcesObjectSchema();
        linksSchema.set$ref("#/components/schemas/Links");

        switch (schemaType) {
            case KEY:
                this.setProperties(keySchema.getProperties());
                this.setRequired(keySchema.getRequired());
                this.set$ref(keySchema.get$ref());
                this.setType(keySchema.getType());
                this.setNeedsKeyMethods(keySchema.getNeedsKeyMethods());
                this.setCanBeRelation(keySchema.getCanBeRelation());
                break;
            case RELATION:
                this.addProperties("data", keySchema)
                    .addProperties("links", linksSchema);
                this.setType("object");
                this.setNullable(true);
                break;
            case RELATION_COLLECTION:
                ArraySchema relations = new ArraySchema();
                relations.setItems(keySchema);
                this.addProperties("data", relations)
                    .addProperties("links", linksSchema);
                this.addRequiredItem("links");
                this.setType("object");
                this.setNullable(true);
                this.setJsonable(true);
                List<String> req = new ArrayList<>();
                req.add("data");
                this.setRequired(req);
                break;
            case DETAILS:
                this.addProperties("temp", new RegourcesStringSchema());
                this.setAdditionalProperties(Boolean.TRUE);
                this.setIsDetails(true);
                this.setNeedsDBMethods(true);
                break;
        }
    }
}