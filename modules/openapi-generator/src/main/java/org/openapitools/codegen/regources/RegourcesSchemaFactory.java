package org.openapitools.codegen.regources;

import java.lang.reflect.InvocationTargetException;

public class RegourcesSchemaFactory {
    @SuppressWarnings("unchecked")
    public static<T> T newInstance(RegourcesSchemaType type) {
        try {
            return  (T) type
                        .getDefaultImplementation()
                        .getDeclaredConstructor(RegourcesSchemaType.class)
                        .newInstance(type);
        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
}
